import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { NavbarComponent } from './navbar/navbar.component';
import { GraficasBarraHorizontalComponent } from './graficas-barra-horizontal/graficas-barra-horizontal.component';

@NgModule({
  declarations: [NavbarComponent, GraficasBarraHorizontalComponent],
  exports: [NavbarComponent, GraficasBarraHorizontalComponent],
  imports: [
    CommonModule,
    RouterModule,
    NgxChartsModule,
    BrowserAnimationsModule,
  ],
})
export class ComponentsModule {}
