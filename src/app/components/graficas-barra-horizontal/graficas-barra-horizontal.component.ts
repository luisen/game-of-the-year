import { Component, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-graficas-barra-horizontal',
  templateUrl: './graficas-barra-horizontal.component.html',
  styleUrls: ['./graficas-barra-horizontal.component.css'],
})
export class GraficasBarraHorizontalComponent implements OnDestroy {
  results: any[] = [
    {
      name: 'Juego 1',
      value: 20,
    },
    {
      name: 'Juego 2',
      value: 25,
    },
    {
      name: 'Juego 3',
      value: 15,
    },
    {
      name: 'Juego 4',
      value: 30,
    },
  ];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Juegos';
  showYAxisLabel = true;
  yAxisLabel = 'Votos';

  colorScheme = 'nightLights';

  interval;

  constructor() {
    this.interval = setInterval(() => {
      console.log('tick');

      const newResults = [...this.results];

      for (let i in newResults) {
        newResults[i].value = Math.round(Math.random() * 500);
      }

      this.results = [...newResults];
    }, 1500);
  }

  onSelect(event): void {
    console.log(event);
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }
}
